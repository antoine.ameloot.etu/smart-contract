> Antoine AMELOOT <antoine.ameloot.etu@univ-lille.fr>  
> BUT 3 Informatique - Groupe M  
> IUT de Lille, site de Villeneuve d'Ascq  
> Octobre 2023  

## Description

Système de vote sous forme de contrat intelligent développé en Solidity.

## Correspondance aux attendus

✅ Le nom du contrat est "Voting".  
✅ Le contrat utilise la dernière version du compilateur.  
✅ L'administrateur (*chairperson*) est la personne déployant le contrat.  
✅ Le contrat définit les structure *Voter* et *Proposal*.  
✅ Le contrat définit une énumération *WorkflowStatus* correspondant aux étapes du processus de vote.  
✅ Le contrat définit une variable *winningProposalId* représentant l'identifiant du gagnant.  
✅ Le contrat définit une fonction *getWinner()* qui donne le gagnant.  
✅ Le contrat utilise le contrat *Ownable* de OpenZeppelin.  
✅ Le contrat définit les évènements *VoterRegistered(address voterAddress)*, *WorkflowStatusChange(WorkflowStatus previousStatus, WorkflowStatus newStatus)*,
*ProposalRegistered(uint proposalId)* et
*Voted (address voter, uint proposalId)*.  

## Fonctionnalités supplémentaires

1. Obtenir la liste des propositions.
2. Attribuer un poids à chaque votant.
3. Obtenir le taux d'abstention à l'issu du vote.

## Live démo

La démonstration est disponible à l'adresse suivante : https://youtu.be/dQJgYESzUYU.